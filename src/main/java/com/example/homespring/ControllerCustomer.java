package com.example.homespring;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
@RestController
@RequestMapping ("/api/v1/customers/")
public class ControllerCustomer {
    List<Customer> customers= new ArrayList<>();
    ControllerCustomer(){
        customers.add(new Customer(1 ,"Dara","M",18,"KPS"));
        customers.add(new Customer(2 ,"Ngorn","M",18,"PP"));
        customers.add(new Customer(3 ,"NuNu","F",18,"PP"));
    }
    @GetMapping("/")
    public  ResponseEntity<Response<List<Customer>>>  getAll(){
        Response<List<Customer>> response = new Response();
        response.setMessage("This record has found successfully");
        response.setCustomer(customers);
        response.setStatus("Ok");
        response.setDateTime(LocalDateTime.now());
        return ResponseEntity.ok(response );
    }
    @GetMapping("/{customerId}")
    public  ResponseEntity<?>  getById(@PathVariable  int customerId){
            Customer cust = new Customer();
        for(Customer customer : customers){
            if(customer.getId() == customerId){
                cust = customer;
                Response<Customer> response = new Response();
                response.setMessage("This record has found successfully");
                response.setCustomer(cust);
                response.setStatus("Ok");
                response.setDateTime(LocalDateTime.now());
                return ResponseEntity.ok(response);
            }
        }
        return  ResponseEntity.notFound().build();
    }
    @GetMapping("/search")
    public  ResponseEntity<Customer>getByName(@RequestParam String name){
        for(Customer customer1 : customers){
            if(customer1.getName().contains(name)){
                return ResponseEntity.ok(customer1);
            }
        }
        return ResponseEntity.notFound().build();
    }
    @PostMapping(path = "/")
    public SaveCustomer insert(@RequestBody SaveCustomer saveCustomer){
        int id = customers.size();
        Customer customer = new Customer();
        customer.setName(saveCustomer.getName());
        customer.setGender(saveCustomer.getGender());
        customer.setAge(saveCustomer.getAge());
        customer.setId(id+1);
        customers.add(customer);
        return saveCustomer;
    }
    @PutMapping("/{customerId}")
    public  ResponseEntity<Response<Customer>>  updateById(@PathVariable int customerId, @RequestBody SaveCustomer update){
        for(Customer customer : customers){
            if(customer.getId() == customerId){
                 customer.setName(update.getName());
                 customer.setGender(update.getGender());
                 customer.setAge(update.getAge());
                 customer.setAddress(update.getAddress());
                Response<Customer> response = new Response();
                response.setMessage("You're update successfully");
                response.setCustomer(customer);
                response.setStatus("Ok");
                response.setDateTime(LocalDateTime.now());
                return ResponseEntity.ok(response );

            }
        }
        return ResponseEntity.notFound().build();
    }
    @DeleteMapping("/{customerId}")
    public  ResponseEntity<?>  DeleteById(@PathVariable int customerId) {
        for(Customer customer : customers){
            if(customer.getId() == customerId){
                customers.remove(customer);
                Map<String,String > response = new HashMap<>();
                response.put("message:","Congratulation your delete is successfully");
                response.put("Status:","Ok");
                response.put("Time:",LocalDateTime.now().toString());
                return ResponseEntity.ok(response);
            }
        }
        return ResponseEntity.notFound().build() ;
}


}
