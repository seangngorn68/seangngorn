package com.example.homespring;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.time.LocalDateTime;

public class Response<T> {
    private String message;
//    @JsonInclude(JsonInclude.Include.NON_NULL)
    private T customer;
    private String status;
    private LocalDateTime dateTime;
   public Response(String message,T customer,String status,LocalDateTime dateTime){
        this.message=message;
        this.customer=customer;
        this.status=status;
        this.dateTime=dateTime;
    }
    public Response() {}

    public String getMessage() {
        return message;
    }
    public void setMessage(String message) {
        this.message = message;
    }
    public T getCustomer() {
        return customer;
    }
    public void setCustomer(T customer) {
        this.customer = customer;
    }
    public String getStatus() {
        return status;
    }
    public void setStatus(String status) {
        this.status = status;
    }
    public LocalDateTime getDateTime() {
        return dateTime;
    }
    public void setDateTime(LocalDateTime dateTime) {
        this.dateTime = dateTime;
    }
}
