package com.example.homespring;

public class SaveCustomer {
    private String name;
    private String  gender;
    private int age;
    private  String address;
    public SaveCustomer( String name,String gender,int age,String  address) {
        this.name = name;
        this.gender = gender;
        this.age = age;
        this.address = address;
    }
        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getGender() {
            return gender;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }

        public int getAge() {
            return age;
        }

        public void setAge(int age) {
            this.age = age;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
    }
}
